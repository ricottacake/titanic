import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()

    mr = df[df['Name'].str.contains(r'Mr\.', case=False, regex=True)]
    mrs = df[df['Name'].str.contains(r'Mrs\.', case=False, regex=True)]
    miss = df[df['Name'].str.contains(r'Miss\.', case=False, regex=True)]

    mr_missing = mr['Age'].isna().sum()
    mrs_missing = mrs['Age'].isna().sum()
    miss_missing = miss['Age'].isna().sum()

    return [('Mr.', mr_missing, mr['Age'].median()), ('Mrs.', mrs_missing, mrs['Age'].median()),
     ('Miss.', miss_missing, miss['Age'].median())]
